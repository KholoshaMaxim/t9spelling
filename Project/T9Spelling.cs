﻿using System;
using System.Collections.Generic;

namespace T9_Spelling
{
    class T9Spelling
    {
        private const long PAUSE = 10000000;
        private string text;
        private Dictionary<int, List<char>> dictionary;
        private long lastTime;
        private int currentKey;
        private int countClick;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public T9Spelling()
        {        
            text = "";
            lastTime = DateTime.Now.Ticks;
            currentKey = -1;
            countClick = 0;
            dictionary = new Dictionary<int, List<char>>();
            dictionary.Add(0, new List<char>() { '+', ' ' });
            dictionary.Add(1, new List<char>() { '.', ',', '!', '?' });
            dictionary.Add(2, new List<char>() { 'A', 'B', 'C' });
            dictionary.Add(3, new List<char>() { 'D', 'E', 'F' });
            dictionary.Add(4, new List<char>() { 'G', 'H', 'I' });
            dictionary.Add(5, new List<char>() { 'J', 'K', 'L' });
            dictionary.Add(6, new List<char>() { 'M', 'N', 'O' });
            dictionary.Add(7, new List<char>() { 'P', 'Q', 'R', 'S' });
            dictionary.Add(8, new List<char>() { 'T', 'U', 'V' });
            dictionary.Add(9, new List<char>() { 'W', 'X', 'Y', 'Z' });
        }

        public string changeText(int key)
        {
            string resultString;
            long time = DateTime.Now.Ticks;

            if (((time - lastTime) > PAUSE) || (currentKey != key))
            {
                countClick = 0;
                resultString = addSymbol(dictionary[key][countClick % dictionary[key].Count]);
            }
            else
            {
                countClick++;
                resultString = replaceLastSymbol(dictionary[key][countClick % dictionary[key].Count]);
            }

            currentKey = key;
            lastTime = DateTime.Now.Ticks;
            return resultString;
        }

        private string addSymbol(char c)
        {
            return text += c;
        }

        private string replaceLastSymbol(char c)
        {
            if (text.Length == 0)
                return text += c;
            else
                return text = text.Remove(text.Length - 1) + c;
        }

        public string removeLastSymbol()
        {
            if (text.Length > 0)
                return text = text.Remove(text.Length - 1);
            
            return text;
        }

        public string removeAllSymbols()
        {
            return text = "";
        }
    }
}
