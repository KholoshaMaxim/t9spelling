﻿using System;
using System.Windows.Forms;

namespace T9_Spelling
{
    public partial class MainForm : Form
    {
        private T9Spelling t9spelling;

        public MainForm()
        {
            InitializeComponent();
            t9spelling = new T9Spelling();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(0);
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(1);
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(2);
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(3);
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(4);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(5);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(6);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(7);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(8);
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.changeText(9);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.removeLastSymbol();
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            txtOutput.Text = t9spelling.removeAllSymbols();
        }
    }
}
