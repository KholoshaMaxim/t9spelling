﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9_Spelling;
using System.Diagnostics;
using System.Windows.Forms;

namespace T9_SpellingTests
{
    [TestClass]
    public class T9_SpellingTests
    {
        [TestMethod]
        public void changeText_2AndAfterPauseTime2_AAreturned()
        {
            int key = 2;
            string expected = "AA";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.changeText(key);
            _pause(3000);
            string actual = t9spelling.changeText(key);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void changeText_2AndBeforePauseTime2_Breturned()
        {
            int key = 2;
            string expected = "B";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.changeText(key);
            string actual = t9spelling.changeText(key);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void changeText_2AndAfterPauseTime3_ADreturned()
        {
            int currentKey = 2;
            int newKey = 3;
            string expected = "AD";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.changeText(currentKey);
            _pause(3000);
            string actual = t9spelling.changeText(newKey);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void changeText_2AndBeforePauseTime3_ADreturned()
        {
            int currentKey = 2;
            int newKey = 3;
            string expected = "AD";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.changeText(currentKey);
            string actual = t9spelling.changeText(newKey);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void removeLastSymbol_textWithoutLastSymbolReturned()
        {
            string text = "TEXT";
            string expected = "TEX";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.Text = text;
            string actual = t9spelling.removeLastSymbol();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void removeLastSymbol_emptyText_EmptyStringReturned()
        {
            string text = "";
            string expected = "";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.Text = text;
            string actual = t9spelling.removeLastSymbol();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void removeAllSymbols_EmptyStringReturned()
        {
            string text = "TEXT";
            string expected = "";

            T9Spelling t9spelling = new T9Spelling();
            t9spelling.Text = text;
            string actual = t9spelling.removeAllSymbols();

            Assert.AreEqual(expected, actual);
        }

        private void _pause (int value)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (sw.ElapsedMilliseconds < value)
                Application.DoEvents();
        }
    }
}
